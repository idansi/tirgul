<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\behaviors\BlameableBehavior;



/* @var $this yii\web\View */
/* @var $model app\models\Article */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="article-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'author_id')->textInput() ?>

    <?= $form->field($model, 'editor_id')->textInput() ?>

    
    
    
	<?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(app\models\Category::find()->asArray()->all(), 'id', 'name'), 
	         ['prompt'=>'-Choose a Category-',
			  'onchange'=>'
				$.post( "'.Yii::$app->urlManager->createUrl('post/lists?id=').'"+$(this).val(), function( data ) {
				  $( "select#title" ).html( data );
				});
			']); 
	
	$dataPost=ArrayHelper::map(\app\models\Category::find()->asArray()->all(), 'id', 'title');
	echo $form->field($model, 'title')
        ->dropDownList(
            $dataPost,           
            ['id'=>'title']
        ); ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'updated_by')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>