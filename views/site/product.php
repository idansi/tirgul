<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use app\models\Product;

$this->title = 'Product';
$this->params['breadcrumbs'][] = $this->title;

$query=Product::find();

 $provider = new ActiveDataProvider([
        'query' => $query
   ]);
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

  <?=
  GridView::widget([
      'dataProvider'=>$provider,
      'columns'=>[
          'id',
          'title'
      ]
  ])
   ?>

    <code><?= __FILE__ ?></code>
</div>
